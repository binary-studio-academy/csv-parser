import CartParser from './CartParser';
import { readFileSync } from 'fs';
import path from 'path';

const testCSV = path.resolve(__dirname, "../samples/cart.csv");
const testJSON = path.resolve(__dirname, "../samples/cart.json");
let cartParser;

beforeEach(() => {
	cartParser = new CartParser();
});

describe('CartParser - unit tests', () => {
	it('Validates should return an empty array', () => {
		expect(cartParser.validate(readFileSync(testCSV, 'utf-8', 'r'))).toEqual([]);
	});

	it('Validates should return an array with errors if a non-csv file is passed', () => {
		expect(cartParser.validate(readFileSync(testJSON, 'utf-8', 'r'))).toEqual(expect.arrayContaining([expect.any(Object)]));
	});

	it('Validates should return an array with errors if headers are not correct', () => {
		const brokenLine = `ProductName,Prise,Quantiti`;

		expect(cartParser.validate(brokenLine)).toEqual(expect.arrayContaining([expect.objectContaining({ type: cartParser.ErrorType.HEADER })]));
	})

	it('Validates should return an array with errors if row are not correct', () => {
		const brokenLine = `Product name,Price,Quantity\nMollis consequat,9.00`;

		expect(cartParser.validate(brokenLine)).toEqual(expect.arrayContaining([expect.objectContaining({ type: cartParser.ErrorType.ROW })]));
	})

	it('Validates should return an array with errors if an empty string is passed as a value', () => {
		const brokenLine = `Product name,Price,Quantity\n,9.00,2`;

		expect(cartParser.validate(brokenLine)).toEqual(expect.arrayContaining([expect.objectContaining({ type: cartParser.ErrorType.CELL })]));
	})

	it('Validates should return an array with errors if the number is not positive', () => {
		const brokenLine = `Product name,Price,Quantity\nMollis consequat,9.00,-2`;

		expect(cartParser.validate(brokenLine)).toEqual(expect.arrayContaining([expect.objectContaining({ message: "Expected cell to be a positive number but received \"-2\"." })]));
	})

	it('ParseLine should return an object with the values id, name, price, quantity', () => {
		const content = readFileSync(testCSV, 'utf-8', 'r');
		const lines = content.split(/\n/).filter(l => l).filter((l, i) => i > 0);

		expect(cartParser.parseLine(lines[0])).toMatchObject({
			id: expect.any(String),
			name: expect.any(String),
			price: expect.any(Number),
			quantity: expect.any(Number)
		});
	});

	it('calcTotal should return a number', () => {
		const result = cartParser.calcTotal([{ price: 12,quantity: 5 },{ price: 7, quantity: 2 }])

    expect(result).toEqual(expect.any(Number));
	});

});

describe('CartParser - integration test', () => {
	// Add your integration test here.
	test('Parse works correctly', () => {
		expect(cartParser.parse(testCSV)).toMatchObject({
			items: expect.any(Array),
			total: expect.any(Number)
		});

		console.error = jest.fn();
		expect(() => cartParser.parse(testJSON)).toThrow();
	})
});